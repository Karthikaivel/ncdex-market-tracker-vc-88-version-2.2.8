import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';



import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SuperTabsModule } from 'ionic2-super-tabs';
import { DtabsPage } from '../pages/dtabs/dtabs';
import { AccountPage } from '../pages/account/account';
import { HolidaysPage } from '../pages/holidays/holidays';
import { FaqPage } from '../pages/faq/faq';
import { RatemePage } from '../pages/rateme/rateme';
import { AboutPage } from '../pages/about/about';
import { NetPage } from '../pages/net/net';
import { RegisterPage } from '../pages/register/register';


import { ComProvider } from '../providers/com/com';
import { HelpPage } from '../pages/help/help';
import { SocialSharing } from '@ionic-native/social-sharing';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { EmailComposer } from '@ionic-native/email-composer';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Device } from '@ionic-native/device';
import { Push } from '@ionic-native/push';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { TermsPage } from '../pages/terms/terms';



@NgModule({
  declarations: [
    MyApp,
    DtabsPage,
    AccountPage,
    HolidaysPage,
    FaqPage,
    RatemePage,
    HelpPage,
    AboutPage,
    NetPage,
    RegisterPage,
    TermsPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    SuperTabsModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    DtabsPage,
    AccountPage,
    HolidaysPage,
    FaqPage,
    RatemePage,
    HelpPage,
    AboutPage,
    NetPage,
    RegisterPage,
    TermsPage
  
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ComProvider,
    SocialSharing,
    EmailComposer,
    InAppBrowser,
    Device,
    Push,
    Geolocation,
    NativeGeocoder
  ]
})
export class AppModule {}
